provider "aws" {
    region = "us-east-2"  
}

resource "aws_instance" "nexus" {
  ami = "ami-0d8f6eb4f641ef691"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ "sg-02c9d665e8b4ffd69" ]
  key_name = "deployer-key"
  tags = {
    env = "ci"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkWB3ff5qcMBZGMQbZ3pH6n9IDnzsVUrJFzjDtQVPvlIuQmvC1M2gFEisCSgyo9fRbuETI+RT1SnjKnUxwQLi2DNixq82DTqzA0jbI2prO8cMFDorTE+ngBOQYQ9fdhqgEfF5llq37gJbWi2R1yBHMcW1cWfuwbsM2t6sl4CkkIKhKeHGvz/UjZR7GzToyXM9kMSZ68njv+9BaN09MoxDn6wYqsItL3zBhQGyX8SLYZ77EZQiFPyaFYBTrA1tJJ3Q1vCWuZGSLGuIZmY0HQAdJOEAgYQ9yLX0ARGr8ayyerL5tG1apCpAUAztv+9E+/HUvccS6a2OrowoR59W9SFKutqnqAUgZBwzzlsvgqj3oxgVhjjHshdG9qXGPcicpjtmSxnMtydRMy0I2AniOetGjV7+MQQK65U5UsMGZf1mNpugwwH+dtxXY+wlVPG4aSsZSqmFv8irx5Aia7o0LwLi3YMUUE9rHD0zezlYIVHtGyHKtcAW7uimn2gL1agz754U= kkirillov@localhost.localdomain"
}


output "public_ip" {
  value = aws_instance.nexus.public_ip
}
