#!/usr/bin/env bash
PROJECT_NAME="gak"

if [[ "$PROJECT_NAME" != $(oc project -q) ]]; then
    oc new-project $PROJECT_NAME
fi    
export RESOURCE_VERSION="$(oc get dc/kb-service -n gak -o yaml | grep '  resourceVersion' | cut -d '"' -f2 | tr -d '\n')"
sed -i "s/5223009/${RESOURCE_VERSION}/g" deploymentConfig.yml
oc apply -f storage.yml && \
    oc apply -f deploymentConfig.yml && \
    oc apply -f svc-4444 && \
    oc apply -f route-4444
