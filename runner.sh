#!/usr/bin/env bash

docker build -t glcirunner -f Dockerfile.gitlab.runner .
docker run -i -t --rm --name gitlab_runner \
       -v /var/run/docker.sock:/var/run/docker.sock \
       --env-file=.env \
       glcirunner bash