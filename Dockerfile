FROM wachiwi/openkb
RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN chmod 777 -R /var/openKB
COPY target/*.jar /var/openKB/app.jar
ENTRYPOINT java -jar app.jar && npm start